import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

const PREFIX_URL = 'https://api.github.com/repos/angular/angular-cli';

@Injectable()
export class ListService {

  constructor(private http: HttpClient) { }

  getAllIssues(): Observable<any[]> {
    // https://api.github.com/repos/angular/angular-cli/issues
    return this.http.get<any[]>(`${PREFIX_URL}/issues`);
  }

  getIssue(): Observable<any> {
    return this.http.get<any>(`${PREFIX_URL}/issues/1`);
  }

}
