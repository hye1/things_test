import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { IssueListComponent } from './issue-list/issue-list.component';
import { IssueDetailComponent } from './issue-detail/issue-detail.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ListService } from './http/list.service';

@NgModule({
  declarations: [
    AppComponent,
    IssueListComponent,
    IssueDetailComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: 'list', component: IssueListComponent},
      {path: 'detail', component: IssueDetailComponent},
      {path: '', redirectTo: '/list', pathMatch: 'full'},
      {path: '**', component: PageNotFoundComponent}
    ]),
  ],
  providers: [
    ListService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
