export interface Issue {
    url: string;            // https://api.github.com/repos/angular/angular-cli/issues/18723,
    // repository_url: string; //https://api.github.com/repos/angular/angular-cli,
    // labels_url: string; //https://api.github.com/repos/angular/angular-cli/issues/18723/labels{/name},
    // comments_url: string; //https://api.github.com/repos/angular/angular-cli/issues/18723/comments,
    // events_url: string; // https://api.github.com/repos/angular/angular-cli/issues/18723/events,
    // html_url: string; //https://github.com/angular/angular-cli/pull/18723,
    // id: 695644993,
    // node_id: string; //MDExOlB1bGxSZXF1ZXN0NDgxODU2MTky,
    // number: 18723,
    // title:  string; //build: update postcss-loader to version 4.0.0 and packages sync up,
    // user: {
    //     login: string; //alan-agius4,
    //     id: 17563226,
    //     node_id: string; //MDQ6VXNlcjE3NTYzMjI2,
    //     avatar_url: string; //https://avatars3.githubusercontent.com/u/17563226?v=4,
    //     gravatar_id: string; //,
    //     url: string; //https://api.github.com/users/alan-agius4,
    //     html_url: string; //https://github.com/alan-agius4,
    //     followers_url: string; //https://api.github.com/users/alan-agius4/followers,
    //     following_url: string; //https://api.github.com/users/alan-agius4/following{/other_user},
    //     gists_url:  string; //https://api.github.com/users/alan-agius4/gists{/gist_id},
    //     starred_url: string; // https://api.github.com/users/alan-agius4/starred{/owner}{/repo},
    //     subscriptions_url: https://api.github.com/users/alan-agius4/subscriptions,
    //     organizations_url: https://api.github.com/users/alan-agius4/orgs,
    //     repos_url: https://api.github.com/users/alan-agius4/repos,
    //     events_url: https://api.github.com/users/alan-agius4/events{/privacy},
    //     received_events_url: https://api.github.com/users/alan-agius4/received_events,
    //     type: User,
    //     site_admin: false
    // },
    // labels: [
    //     {
    //     id: 393399567,
    //     node_id: MDU6TGFiZWwzOTMzOTk1Njc=,
    //     url: https://api.github.com/repos/angular/angular-cli/labels/cla:%20yes,
    //     name: cla: yes,
    //     color: 0e8a16,
    //     default: false,
    //     description: null
    //     }
    // ],
    // state: open,
    // locked: false,
    // assignee: null,
    // assignees: [

    // ],
    // milestone: null,
    // comments: 0,
    // created_at: 2020-09-08T07:55:02Z,
    // updated_at: 2020-09-08T08:49:54Z,
    // closed_at: null,
    // author_association: COLLABORATOR,
    // active_lock_reason: null,
    // pull_request: {
    //     url: https://api.github.com/repos/angular/angular-cli/pulls/18723,
    //     html_url: https://github.com/angular/angular-cli/pull/18723,
    //     diff_url: https://github.com/angular/angular-cli/pull/18723.diff,
    //     patch_url: https://github.com/angular/angular-cli/pull/18723.patch
    // },
    // body: ,
    // performed_via_github_app: null
}
